from Model.catalog import catalog
from XmlSerializer import XmlSerializer

if __name__ == '__main__':
    xmlFile = open('Books.xml')
    xmlContent = xmlFile.read()

    xmlSerializer = XmlSerializer()
    catalogObject = xmlSerializer.deserialize(xmlContent, catalog)

    for book in catalogObject.books:
        print("Book id: " + book.id)
        print("\tAuthor: " + book.author)
        print("\tTitle: " + book.title)
        print("\tGenre: " + book.genre)
        print("\tPrice: " + str(book.price))
        print("\tPublish date: " + book.publish_date)
        print("\tDescription: " + book.description)
        print("\n")