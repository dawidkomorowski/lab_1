from XmlSerializer import XmlAttribute, XmlValue


class book:
    id = XmlAttribute()
    author = XmlValue()
    title = XmlValue()
    genre = XmlValue()
    price = XmlValue(float)
    publish_date = XmlValue()
    description = XmlValue()
