import xml.etree.ElementTree as ElementTree
from xml.dom import minidom


class XmlSerializer:
    class Formatting:
        Minimal = 0
        Pretty = 1

    @staticmethod
    def deserialize(xml, type):
        deserializedObject = None
        root = ElementTree.fromstring(xml)

        if type.__name__ == root.tag:
            deserializedObject = type()
            XmlSerializer.__fillObjectWIthAttributes(deserializedObject, root)
            XmlSerializer.__fillObjectWithValues(deserializedObject, root)
            XmlSerializer.__fillObjectWithCollections(deserializedObject, root)

        return deserializedObject

    @staticmethod
    def serialize(object, type, formatting=Formatting.Minimal):
        serializedObject = None

        if type.__name__ == object.__class__.__name__:
            xmlRoot = ElementTree.Element(type.__name__)
            XmlSerializer.__fillXmlElementWithAttributes(xmlRoot, object)
            XmlSerializer.__fillXmlElementWithValues(xmlRoot, object)
            XmlSerializer.__fillXmlElementWithCollection(xmlRoot, object)
            serializedObject = ElementTree.tostring(xmlRoot)
            serializedObject = XmlSerializer.prettefiy(serializedObject)

        return serializedObject

    @staticmethod
    def prettefiy(xml):
        parsed = minidom.parseString(xml)
        return parsed.toprettyxml(indent='\t')

    @staticmethod
    def __fillObjectWIthAttributes(object, xmlElement):
        attributes = xmlElement.attrib
        for key in attributes:
            if hasattr(object, key):
                member = getattr(object, key)
                if isinstance(member, XmlAttribute):
                    converter = member.converter
                    setattr(object, key, converter(attributes[key]))

    @staticmethod
    def __fillObjectWithValues(object, xmlElement):
        for child in xmlElement:
            tag = child.tag
            if hasattr(object, tag):
                member = getattr(object, tag)
                if isinstance(member, XmlValue):
                    converter = member.converter
                    setattr(object, tag, converter(child.text))

    @staticmethod
    def __fillObjectWithCollections(object, xmlElement):
        membersNames = dir(object)
        for memberName in membersNames:
            member = getattr(object, memberName)
            if isinstance(member, XmlCollection):
                type = member.type
                member = ()
                for child in xmlElement:
                    tag = child.tag
                    if type.__name__ == tag:
                        deserializedMember = type()
                        XmlSerializer.__fillObjectWIthAttributes(deserializedMember, child)
                        XmlSerializer.__fillObjectWithValues(deserializedMember, child)
                        XmlSerializer.__fillObjectWithCollections(deserializedMember, child)
                        member = member + (deserializedMember,)

                setattr(object, memberName, member)

    @staticmethod
    def __fillXmlElementWithAttributes(xmlElement, object):
        membersNames = dir(object)
        for memberName in membersNames:
            member = getattr(object.__class__, memberName)
            if isinstance(member, XmlAttribute):
                xmlElement.attrib[memberName] = str(getattr(object, memberName))

    @staticmethod
    def __fillXmlElementWithValues(xmlElement, object):
        membersNames = dir(object)
        for memberName in membersNames:
            member = getattr(object.__class__, memberName)
            if isinstance(member, XmlValue):
                childElement = ElementTree.SubElement(xmlElement, memberName)
                childElement.text = str(getattr(object, memberName))

    @staticmethod
    def __fillXmlElementWithCollection(xmlElement, object):
        membersNames = dir(object)
        for memberName in membersNames:
            classMember = getattr(object.__class__, memberName)
            if isinstance(classMember, XmlCollection):
                member = getattr(object, memberName)
                for childObject in member:
                    childElement = ElementTree.SubElement(xmlElement, classMember.type.__name__)
                    XmlSerializer.__fillXmlElementWithAttributes(childElement, childObject)
                    XmlSerializer.__fillXmlElementWithValues(childElement, childObject)
                    XmlSerializer.__fillXmlElementWithCollection(childElement, childObject)


class XmlAttribute:
    def __init__(self, converter=str):
        self.converter = converter


class XmlValue:
    def __init__(self, converter=str):
        self.converter = converter


class XmlCollection:
    def __init__(self, type):
        self.type = type
