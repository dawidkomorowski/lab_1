from LoggingDecorators import log
from Writers.AbstractWriterFactory import AbstractWriterFactory
from Writers.WriterFactory import WriterFactory


class MockWriterFactory(AbstractWriterFactory):
    @log(__name__)
    def __init__(self, name, arg = None):
        self.name = name
        self.arg = arg
        self.writerFactory = WriterFactory()
        self.lastCreatedWriter = None

    @log(__name__)
    def create(self, name, arg = None):
        self.lastCreatedWriter = self.writerFactory.create(self.name, self.arg)
        return self.lastCreatedWriter
