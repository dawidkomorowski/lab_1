from abc import abstractmethod


class AbstractWriterFactory:
    @abstractmethod
    def create(self,name, arg = None):
        pass