from LoggingDecorators import log
from Writers.AbstractWriter import AbstractWriter


class FileWriter(AbstractWriter):
    @log(__name__)
    def __init__(self, filepath):
        self.filepath = filepath
        self.file = open(self.filepath, "w")

    @log(__name__)
    def __enter__(self):
        return self

    @log(__name__)
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()

    @log(__name__)
    def write(self, message):
        self.file.write(message + "\n")
