from LoggingDecorators import log
from Writers.AbstractWriter import AbstractWriter


class ConsoleWriter(AbstractWriter):
    @log(__name__)
    def __enter__(self):
        return self

    @log(__name__)
    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    @log(__name__)
    def write(self,message):
        print(message)