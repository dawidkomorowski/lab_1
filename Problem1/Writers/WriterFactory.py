from LoggingDecorators import log
from Writers.AbstractWriterFactory import AbstractWriterFactory
from Writers.ConsoleWriter import ConsoleWriter
from Writers.FileWriter import FileWriter
from Writers.StringWriter import StringWriter


class WriterFactory(AbstractWriterFactory):
    UnsupportedTypeExceptionMessage = "No corresponding writer class for given name: "

    @log(__name__)
    def create(self, name, arg = None):
        if name == str(ConsoleWriter):
            return ConsoleWriter()
        if name == str(FileWriter):
            return FileWriter(arg)
        if name == str(StringWriter):
            return StringWriter()
        raise Exception(WriterFactory.UnsupportedTypeExceptionMessage + name)