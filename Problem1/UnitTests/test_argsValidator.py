from unittest import TestCase

from ArgsValidators.ArgsValidator import ArgsValidator
from FileServices.MockFileService import MockFileService


class TestArgsValidator(TestCase):
    def test_validate_ShouldPassValidation_GivenArgumentBeingExistingFile(self):
        # Arrange
        mockFileService = MockFileService(True)
        argsValidator = ArgsValidator(mockFileService)
        args = ["main.py", "main.py"]

        # Act
        # Assert
        argsValidator.validate(args)

    def test_validate_ShouldPassValidation_GivenTwoArgumentsAndFirstArgumentBeingExistingFile(self):
        # Arrange
        mockFileService = MockFileService(True)
        argsValidator = ArgsValidator(mockFileService)
        args = ["main.py", "main.py", "output.txt"]

        # Act
        # Assert
        argsValidator.validate(args)

    def test_validate_ShouldRaiseException_GivenTooFewArguments(self):
        # Arrange
        mockFileService = MockFileService(True)
        argsValidator = ArgsValidator(mockFileService)
        args = ["main.py"]

        # Act
        with self.assertRaises(Exception) as cm:
            argsValidator.validate(args)
        ex = cm.exception

        # Assert
        self.assertEqual(str(ex), ArgsValidator.ArgsNoExceptionMessage)

    def test_validate_ShouldRaiseException_GivenTooManyArguments(self):
        # Arrange
        mockFileService = MockFileService(True)
        argsValidator = ArgsValidator(mockFileService)
        args = ["main.py", "main.py", "output.txt", "-v"]

        # Act
        with self.assertRaises(Exception) as cm:
            argsValidator.validate(args)
        ex = cm.exception

        # Assert
        self.assertEqual(str(ex), ArgsValidator.ArgsNoExceptionMessage)

    def test_validate_ShouldRaiseException_GivenArgumentNotBeingExistingFile(self):
        # Arrange
        mockFileService = MockFileService(False)
        argsValidator = ArgsValidator(mockFileService)
        args = ["main.py", "main.py"]

        # Act
        with self.assertRaises(Exception) as cm:
            argsValidator.validate(args)
        ex = cm.exception

        # Assert
        self.assertEqual(str(ex), ArgsValidator.FirstArgNotAFileExceptionMessage)
