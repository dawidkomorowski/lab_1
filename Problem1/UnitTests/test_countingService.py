from unittest import TestCase

from CountingServices.CountingService import CountingService
from UnitTests.TestData import TestData


class TestCountingService(TestCase):
    chars = TestData.chars
    lines = TestData.lines
    words = TestData.words
    data = TestData.string

    def test_countChars(self):
        # Arrange
        countingService = CountingService()

        # Act
        result = countingService.countChars(TestCountingService.data)

        # Assert
        self.assertEqual(result,TestCountingService.chars)

    def test_countLines(self):
        # Arrange
        countingService = CountingService()

        # Act
        result = countingService.countLines(TestCountingService.data)

        # Assert
        self.assertEqual(result,TestCountingService.lines)

    def test_countWords(self):
        # Arrange
        countingService = CountingService()

        # Act
        result = countingService.countWords(TestCountingService.data)

        # Assert
        self.assertEqual(result,TestCountingService.words)
