import os
from unittest import TestCase
from ArgsValidators.MockArgsValidator import MockArgsValidator
from CountingServices.MockCountingService import MockCountingService
from FileServices.MockFileService import MockFileService
from UnitTests.TestData import TestData
from WordCounter import WordCounter
from Writers.FileWriter import FileWriter
from Writers.MockWriterFactory import MockWriterFactory
from Writers.StringWriter import StringWriter


class TestWordCounter(TestCase):
    filepath = TestData.filepath
    exception = TestData.exception

    def test_execute_ShouldWriteCorrectOutput(self):
        # Arrange
        mockFileService = MockFileService()
        mockArgsValidator = MockArgsValidator()
        mockCountingService = MockCountingService(30, 10, 20)
        writerFactory = MockWriterFactory(str(StringWriter))
        wordCounter = WordCounter(mockArgsValidator, mockFileService, mockCountingService, writerFactory)
        args = ["main.py", "main.py"]

        # Act
        wordCounter.execute(args)

        # Assert
        self.assertEqual(writerFactory.lastCreatedWriter.output, """In file 'main.py' found
characters: 30
lines: 10
words: 20
""")

    def test_execute_ShouldWriteCorrectOutputToFile(self):
        # Arrange
        mockFileService = MockFileService()
        mockArgsValidator = MockArgsValidator()
        mockCountingService = MockCountingService(30, 10, 20)
        writerFactory = MockWriterFactory(str(FileWriter), TestWordCounter.filepath)
        wordCounter = WordCounter(mockArgsValidator, mockFileService, mockCountingService, writerFactory)
        args = ["main.py", "main.py", TestWordCounter.filepath]

        # Act
        wordCounter.execute(args)

        # Assert
        file = open(TestWordCounter.filepath, "r")
        self.assertEqual(file.read(), """In file 'main.py' found
characters: 30
lines: 10
words: 20
""")
        file.close()
        os.remove(TestWordCounter.filepath)

    def test_execute_ShouldRaiseException_GivenIncorrectArguments(self):
        # Arrange
        mockFileService = MockFileService()
        mockArgsValidator = MockArgsValidator(TestWordCounter.exception)
        mockCountingService = MockCountingService(30, 10, 20)
        writerFactory = MockWriterFactory(str(StringWriter), TestWordCounter.filepath)
        wordCounter = WordCounter(mockArgsValidator, mockFileService, mockCountingService, writerFactory)
        args = ["main.py", "main.py"]

        # Act
        with self.assertRaises(Exception) as cm:
            wordCounter.execute(args)
        ex = cm.exception

        # Assert
        self.assertEqual(str(ex),str(TestWordCounter.exception))