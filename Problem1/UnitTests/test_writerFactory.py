import os
from unittest import TestCase

from UnitTests.TestData import TestData
from Writers.ConsoleWriter import ConsoleWriter
from Writers.FileWriter import FileWriter
from Writers.StringWriter import StringWriter
from Writers.WriterFactory import WriterFactory


class TestWriterFactory(TestCase):
    filepath = TestData.filepath
    def test_create_ShouldReturnCorrectImplementation(self):
        # arrange
        writerFactory = WriterFactory()

        # act
        with writerFactory.create(str(ConsoleWriter)) as consoleWriter:
            pass
        with writerFactory.create(str(FileWriter), TestWriterFactory.filepath) as fileWriter:
            pass
        with writerFactory.create(str(StringWriter)) as stringWriter:
            pass
        os.remove(TestWriterFactory.filepath)

        # assert
        self.assertIsInstance(consoleWriter,ConsoleWriter)
        self.assertIsInstance(fileWriter,FileWriter)
        self.assertIsInstance(stringWriter, StringWriter)

    def test_create_ShouldRaiseException_GivenArgumentForUnsupportedType(self):
        # arrange
        writerFactory = WriterFactory()
        unsupportedType = "UnsupportedType"

        # act
        with self.assertRaises(Exception) as cm:
            writerFactory.create(unsupportedType)
        ex = cm.exception

        # assert
        self.assertEqual(str(ex), WriterFactory.UnsupportedTypeExceptionMessage + unsupportedType)