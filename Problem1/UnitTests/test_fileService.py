import os
from unittest import TestCase
from FileServices.FileService import FileService
from UnitTests.TestData import TestData


class TestFileService(TestCase):
    filepath = TestData.filepath
    fileContent = TestData.string

    def setUp(self):
        file = open(TestFileService.filepath, "w")
        file.write(TestFileService.fileContent)
        file.close()

    def test_isFile_ShouldReturnTrue_WhenFileExists(self):
        # Arrange
        fileService = FileService()

        # Assert
        result = fileService.isFile(TestFileService.filepath)

        # Act
        self.assertTrue(result)

    def test_isFile_ShouldReturnFalse_WhenFileNotExists(self):
        # Arrange
        fileService = FileService()

        # Assert
        result = fileService.isFile(TestFileService.filepath + "_file_which_not_exists")

        # Act
        self.assertFalse(result)

    def test_readFile_ShouldReturnCorrectFileContent(self):
        # Arrange
        fileService = FileService()

        # Assert
        result = fileService.readFile(TestFileService.filepath)

        # Act
        self.assertEqual(result, TestFileService.fileContent)

    def tearDown(self):
        os.remove(TestFileService.filepath)
