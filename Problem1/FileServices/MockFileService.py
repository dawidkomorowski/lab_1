from FileServices.AbstractFileService import AbstractFileService
from LoggingDecorators import log


class MockFileService(AbstractFileService):
    @log(__name__)
    def __init__(self, isFileResult=True, readFileResult=""):
        self.isFileResult = isFileResult
        self.readFileResult = readFileResult

    @log(__name__)
    def isFile(self, filepath):
        return self.isFileResult

    @log(__name__)
    def readFile(self, filepath):
        return self.readFileResult