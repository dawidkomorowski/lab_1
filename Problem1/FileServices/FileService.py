import os.path

from FileServices.AbstractFileService import AbstractFileService
from LoggingDecorators import log


class FileService(AbstractFileService):
    @log(__name__)
    def isFile(self, filepath):
        return os.path.isfile(filepath)

    @log(__name__)
    def readFile(self, filepath):
        file = open(filepath)
        fileContent = file.read()
        file.close()
        return fileContent
