from abc import abstractmethod


class AbstractFileService:
    @abstractmethod
    def isFile(self, filepath):
        pass

    @abstractmethod
    def readFile(self, filepath):
        pass
