from ArgsValidators.AbstractArgsValidator import AbstractArgsValidator
from LoggingDecorators import log


class ArgsValidator(AbstractArgsValidator):
    ArgsNoExceptionMessage = "Incorrect number of arguments. Expected arguments:\n1: input_file_path\n2: input_file_path output_file_path"
    FirstArgNotAFileExceptionMessage = "File not found. Argument value must target existing file."

    @log(__name__)
    def __init__(self, fileService):
        self.fileService = fileService

    @log(__name__)
    def validate(self, args):
        if len(args) < 2 or len(args) > 3:
            raise Exception(ArgsValidator.ArgsNoExceptionMessage)
        if not self.fileService.isFile(args[1]):
            raise Exception(ArgsValidator.FirstArgNotAFileExceptionMessage)
