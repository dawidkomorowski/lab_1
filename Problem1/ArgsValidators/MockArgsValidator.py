from ArgsValidators.AbstractArgsValidator import AbstractArgsValidator
from LoggingDecorators import log


class MockArgsValidator(AbstractArgsValidator):
    @log(__name__)
    def __init__(self, exception = None):
        self.exception = exception

    @log(__name__)
    def validate(self, args):
        if self.exception is not None:
            raise self.exception