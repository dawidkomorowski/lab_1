from abc import abstractmethod


class AbstractArgsValidator:
    @abstractmethod
    def validate(self,args):
        pass