import logging
import sys

from ArgsValidators.ArgsValidator import ArgsValidator
from CountingServices.CountingService import CountingService
from FileServices.FileService import FileService
from WordCounter import WordCounter
from Writers.WriterFactory import WriterFactory

if __name__ == '__main__':
    args = sys.argv
    logging.basicConfig(format='%(asctime)s %(levelname)s %(name)s: %(message)s', level=logging.DEBUG)
    if args.__contains__("-v"):
        args.remove("-v")
        pass
    else:
        logging.disable(logging.ERROR)

    fileService = FileService()
    argsValidator = ArgsValidator(fileService)
    countingService = CountingService()
    writerFactory = WriterFactory()
    wordCounter = WordCounter(argsValidator, fileService, countingService, writerFactory)

    try:
        wordCounter.execute(args)
    except Exception as e:
        print(e)
        logging.getLogger(__name__).exception(e.message)
