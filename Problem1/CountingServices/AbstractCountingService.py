from abc import abstractmethod


class AbstractCountingService:
    @abstractmethod
    def countChars(self, data):
        pass

    @abstractmethod
    def countLines(self, data):
        pass

    @abstractmethod
    def countWords(self, data):
        pass
