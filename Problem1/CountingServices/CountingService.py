import re

from CountingServices.AbstractCountingService import AbstractCountingService
from LoggingDecorators import log


class CountingService(AbstractCountingService):
    @log(__name__)
    def countChars(self, data):
        return len(data)

    @log(__name__)
    def countLines(self, data):
        return len(data.split('\n'))

    @log(__name__)
    def countWords(self, data):
        preprocessed = re.compile("[,.;:!?""']+").sub(" ", data)
        return len(re.compile("\s+").split(preprocessed))
