from CountingServices.AbstractCountingService import AbstractCountingService
from LoggingDecorators import log


class MockCountingService(AbstractCountingService):
    @log(__name__)
    def __init__(self, chars, lines, words):
        self.chars = chars
        self.lines = lines
        self.words = words

    @log(__name__)
    def countChars(self, data):
        return self.chars

    @log(__name__)
    def countLines(self, data):
        return self.lines

    @log(__name__)
    def countWords(self, data):
        return self.words