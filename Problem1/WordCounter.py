from LoggingDecorators import log
from Writers.ConsoleWriter import ConsoleWriter
from Writers.FileWriter import FileWriter


class WordCounter:
    @log(__name__)
    def __init__(self, argsValidator, fileService, countingService, writerFactory):
        self.argsValidator = argsValidator
        self.fileService = fileService
        self.countingService = countingService
        self.writerFactory = writerFactory

    @log(__name__)
    def execute(self, args):
        self.argsValidator.validate(args)
        filepath = args[1]
        fileContent = self.fileService.readFile(filepath)

        chars = self.countingService.countChars(fileContent)
        lines = self.countingService.countLines(fileContent)
        words = self.countingService.countWords(fileContent)

        if len(args) == 2:
            args = (str(ConsoleWriter), )
        else:
            args = (str(FileWriter), args[2])

        with self.writerFactory.create(*args) as writer:
            writer.write("In file '" + filepath + "' found")
            writer.write("characters: " + str(chars))
            writer.write("lines: " + str(lines))
            writer.write("words: " + str(words))
