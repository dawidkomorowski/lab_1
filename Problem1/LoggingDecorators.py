import logging
from functools import wraps

def log(loggerName = __name__, trace = True, args = True, retval = True):
    def wrapper(function):
        @wraps(function)
        def decorator(*dec_args, **kwargs):
            if trace:
                logging.getLogger(loggerName).debug("Entering " + function.__name__ + ".")
            if args:
                logargs(loggerName, function.__name__, dec_args, function.func_code.co_varnames)
            result = function(*dec_args, **kwargs)
            if retval:
                logretval(loggerName,function.__name__,result)
            if trace:
                logging.getLogger(loggerName).debug("Exiting " + function.__name__ + ".")
            return result
        return decorator
    return wrapper

def logargs(loggerName, funcName, args, argsNames):
    output = ""
    for i in range(len(args)):
        output += str(argsNames[i]) + " = " + str(args[i])
        if i < len(args)-1:
            output += "\n"
    logging.getLogger(loggerName).debug(funcName + " args:\n" + output)

def logretval(loggerName, funcName, retval):
    logging.getLogger(loggerName).debug(funcName + " retval: " + str(retval))